# Esri Arcgis layers in Openlayers

Just that. Loads three types of arcgis layers into openlayers with style support
in the case of `FeatureServer` layers.

* Raster (MapServer)
* Vector (FeatureServer)
* VectorTile (VectorTileServer)

## Running

Works with parcel. Just do:

    npm install
    npx parcel index.html
