import Map from 'ol/Map';
import View from 'ol/View';
import XYZ from 'ol/source/XYZ';
import Vector from 'ol/source/Vector';
import VectorTile from 'ol/source/VectorTile';
import {fromLonLat} from 'ol/proj';
import {Tile as TileLayer} from 'ol/layer';
import {Vector as VectorLayer, VectorTile as VectorTileLayer} from 'ol/layer';
import { GeoJSON, MVT } from 'ol/format';

import { createStyleFunctionFromUrl, setMapProjection } from 'ol-esri-style';

const raster = new TileLayer({
  source: new XYZ({
    attributions:
      'Tiles © <a href="https://services.arcgisonline.com/ArcGIS/' +
      'rest/services/World_Topo_Map/MapServer">ArcGIS</a>',
    url:
      'https://server.arcgisonline.com/ArcGIS/rest/services/' +
      'World_Topo_Map/MapServer/tile/{z}/{y}/{x}',
  }),
});

const vectorTileLayer = new VectorTileLayer({
  source: new VectorTile({
    format: new MVT(),
    url: 'https://vectortileservices3.arcgis.com/GVgbJbqm8hXASVYi/arcgis/rest/services/Santa_Monica_Mountains_Parcels_VTL/VectorTileServer/tile/{z}/{y}/{x}.pbf',
  }),
});

const featureLayer = new VectorLayer({
  source: new Vector({
    format: new GeoJSON(),
    url: 'https://services3.arcgis.com/GVgbJbqm8hXASVYi/ArcGIS/rest/services/2020_Earthquakes/FeatureServer/0/query?where=1%3D1&outFields=*&returnGeometry=true&f=geojson',
  }),
});

const view = new View({
  center: fromLonLat([-97.6114, 38.8403]),
  zoom: 7,
});

setMapProjection(view.getProjection());

createStyleFunctionFromUrl('https://services3.arcgis.com/GVgbJbqm8hXASVYi/ArcGIS/rest/services/2020_Earthquakes/FeatureServer/0').then(styleFunction => {
  featureLayer.setStyle(styleFunction);
});

const map = new Map({
  layers: [raster, featureLayer, vectorTileLayer],
  target: document.getElementById('map'),
  view,
});
